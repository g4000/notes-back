FROM node:10
LABEL ownew="SUgroup"
LABEL mantainer="Brenda Nahomi Quispe Gonzales"
ENV TZ =America/Lima
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app-prueba
COPY package*.json ./
RUN npm ci --only=production

COPY . /app-prueba
COPY config.js /app-prueba/config.js

ENV DB_HOST=db
ENV DB_PORT=3306
ENV DB_NAME=notes1107
ENV DB_USER=newuser
ENV DB_PASSWORD=pwd147Hola#

EXPOSE 3040
VOLUME /app-prueba/public

CMD [ "node", "index.js" ]
