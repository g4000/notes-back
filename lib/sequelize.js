const config  = require('../config')
const { Sequelize } = require('sequelize')
const NoteModel = require('../models/note')
const sequelize= new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASSWORD,{
    loggin: false,
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: 'mysql',
    sync: {
        force: false, 
        alter: false
    },
    timezone: "-05:00",
    define: {
        syncOnAssociation: true, 
        charset: 'utf8'
    }
})
const Note = NoteModel(sequelize, Sequelize.DataTypes)
sequelize.sync()
    .then(()=> {
        console.log('Tables created ')
    })

module.exports={
    Note
}