require('dotenv').config()
const express = require('express')
const app = express()
const cors= require('cors')

app.use(express.json())
app.use(cors())

require('./routes')(app)
app.get('/', (req, res) => {
    console.log('Api working v1.0 🙂')
})

app.listen(3040, () => {
    console.log(`SERVER RUNNING ON PORT 3041 🚀 `)
})
