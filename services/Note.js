const { Note} = require('../lib/sequelize')
class NoteService{
    constructor(){}
    async findAll(){
        const query= await Note.findAll({})
        if(query){
            console.log("hola mundo desde prod")
            return query
        }
    }
    async create({ data}) {
		const query = await Note.create({
			title: data.title,
            content: data.content
		});
		return query;
	}


}
module.exports= NoteService
