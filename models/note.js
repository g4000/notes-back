module.exports= (sequelize, DataTypes) =>{
    const Note = sequelize.define('note',  {
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title:  {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        content: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
    },{
        freezeTableName: true,
        timestamps: false,
        tableName: 'notes'
    })
    return Note
}