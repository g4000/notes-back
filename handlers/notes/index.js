const NoteService= require('../../services/Note')
const service = new NoteService()

exports.findAll = async(req, res, next) => {
    try {
        const query = await service.findAll()
        if(!query){
            return res.status(404).send({ message: 'No encontrado'})
        }
        res.send(query)
    } catch (error) {
        next(error)
    }
}
exports.create = async(req, res, next) => {
    try {
        const noteService = new NoteService()
        const params= {
            data: {
                title: req.body.title,
                content: req.body.content
            }
        }
        const query = await noteService.create(params)
        res.status(201).json({"message": "note created"})
    } catch (error) {
        next(error)
    }
}